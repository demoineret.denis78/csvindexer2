const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'mydatabase';

// Options for MongoClient
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

// Function to initialize the cluster
async function initializeCluster() {
  const client = new MongoClient(url, options);

  try {
    // Connect to the MongoDB server
    await client.connect();

    // Connect to admin database to perform admin operations
    const adminDb = client.db('admin').admin();

    // Add each shard to the cluster
    await adminDb.addShard('shard1/host1:port,host2:port,host3:port');
    await adminDb.addShard('shard2/host1:port,host2:port,host3:port');

    console.log('Shards added successfully.');
  } finally {
    // Close the MongoDB connection
    await client.close();
  }
}

// Call the function to initialize the cluster
initializeCluster().catch(console.error);
