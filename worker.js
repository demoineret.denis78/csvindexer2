const fs = require("fs-extra");
const csv = require("convert-csv-to-json");


/*
// Nom du fichier CSV à diviser
const nomFichierCSV = "StockEtablissementHistorique.csv";

// Utiliser la fonction splitCSV pour diviser le fichier CSV
splitCSV(nomFichierCSV)
  .then(csvSplitResponse => {
    // Le fichier CSV a été divisé avec succès
    // Vous pouvez effectuer d'autres opérations ici si nécessaire
    console.log("Division du fichier CSV terminée avec succès :", csvSplitResponse);
  })
  .catch(error => {
    // Une erreur s'est produite lors de la division du fichier CSV
    console.error("Erreur lors de la division du fichier CSV :", error);
  });
*/


const cheminDossierCSV = process.env.TEST;

// Fonction pour compter le nombre de fichiers dans le dossier CSV
const compterFichiersCSV = () => {
  try {
    // Lire le contenu du dossier CSV
    // Compter le nombre de fichiers dans le dossier
    const fichiersDansDossier = fs.readdirSync(cheminDossierCSV);
    const nombreDeFichiers = fichiersDansDossier.length;
    console.log(`Il y a ${nombreDeFichiers} fichiers dans le dossier CSV.`);
    return nombreDeFichiers;
  } catch (erreur) {
    console.error("Erreur lors du comptage des fichiers CSV :", erreur);
    return 0; // En cas d'erreur, retourner 0 fichiers
  }
};


const startTime = Date.now();


// Fonction pour convertir un fichier CSV en JSON
const csvToJson = async (file) => {
  try {
    console.log(`Analyse du fichier ${file}`); // Afficher un message indiquant le début de l'analyse du fichier
    let json = csv.fieldDelimiter(',').getJsonFromCsv(`chunks/csv/${file}`); // Convertir le fichier CSV en JSON
    console.log(`Le fichier ${file} a été analysé`); // Afficher un message indiquant la fin de l'analyse du fichier

    // Supprimer les clés vides dans chaque objet JSON
    json.forEach((item) => {
      for (let key in item) {
        if (item[key] === "") {
          delete item[key];
        }
      }
    });

    // Écrire le JSON résultant dans un fichier
    fs.writeFileSync(`chunks/json/${file}.json`, JSON.stringify(json));

    return; // Retourner une fois que le fichier a été converti avec succès
  } catch (e) {
    console.error(e); // Afficher une erreur s'il y a un problème lors de la conversion
  }
};


// Fonction pour analyser tous les fichiers de CSV
const parseAllFiles = () => {
  try {
    // Appel de la fonction pour compter les fichiers CSV
    console.log("nom last file : ")
    const fichiersDansDossier = fs.readdirSync("C:/Users/demoi/Documents/CsvProject/chunks/csv");
    console.log()
    const nombreDeFichiers = fichiersDansDossier.length;
    const nomLastFile = "/chunks/csv/" + nombreDeFichiers +".csv"
    console.log(`nom last file : ${nomLastFile}`)
    // Parcourir tous les fichiers à partir du numéro de fichier de départ jusqu'au numéro de fichier de fin
    for (let i = 0; i <= nombreDeFichiers; i++) {
      const start = Date.now(); // Enregistrer le temps de départ de l'analyse du fichier actuel
      csvToJson(`${i}.csv`); // Appeler la fonction pour analyser le fichier CSV actuel
      const duree = Date.now() - start; // Calculer la durée de l'analyse du fichier
      const minutes = Math.floor(duree / (1000 * 60)); // Calculer le nombre de minutes écoulées
      const seconds = Math.floor((duree % (1000 * 60)) / 1000); // Calculer le nombre de secondes écoulées
      const milliseconds = duree % 1000; // Calculer le nombre de millisecondes écoulées
      console.log(`Le fichier ${i}.csv a été analysé en ${minutes}:${seconds},${milliseconds}`);
    }
    // Calculer le temps total écoulé pour analyser tous les fichiers
    const duree = Date.now() - startTime;
    const millisecondes = duree % 1000; // Calculer le reste des millisecondes
    const secondes = Math.floor((duree % (1000 * 60)) / 1000); // Calculer le reste des secondes
    const minutes = Math.floor(duree / (1000 * 60)); // Calculer le nombre total de minutes
    console.log(`Tous les fichiers ont été analysés en ${minutes}:${secondes},${millisecondes}`);
  } catch (e) {
    console.error(e); // Afficher une erreur s'il y a un problème lors de l'analyse des fichiers
  }
};

parseAllFiles()
